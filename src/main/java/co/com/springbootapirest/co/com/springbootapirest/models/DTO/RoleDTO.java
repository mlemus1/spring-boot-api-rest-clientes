package co.com.springbootapirest.co.com.springbootapirest.models.DTO;

import co.com.springbootapirest.co.com.springbootapirest.models.Usuario;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.ManyToMany;
import java.io.Serializable;
import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RoleDTO implements Serializable {

    private Long id;
    private String userName;
    private List<Usuario> usuarios;
}
