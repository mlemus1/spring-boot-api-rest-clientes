package co.com.springbootapirest.controllers;


import co.com.springbootapirest.co.com.springbootapirest.models.Cliente;
import co.com.springbootapirest.co.com.springbootapirest.models.DTO.ClienteDTO;
import co.com.springbootapirest.services.IClienteService;
import lombok.extern.log4j.Log4j;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
//import lombok.extern.log4j.log4j2;

//@Log4j
@Log4j2
@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/api")
public class ClienteRestController {
   // private static final Logger log = LogManager.getLogger(ClienteRestController.class.getName());

    @Autowired
    private IClienteService clienteService;

    private final int RESTROS_POR_PAGINA = 10;
    @GetMapping("/clientes")
    public List<Cliente> consultarTodos(){

    return clienteService.findAll();

    }

    @GetMapping("/clientes/page/{page}")
    public Page<Cliente> consultarTodos(@PathVariable Integer page){
        PageRequest pageRequest = PageRequest.of(page, RESTROS_POR_PAGINA);
        return clienteService.findAll(pageRequest);

    }

    @GetMapping("/clientes/{id}")
    public ResponseEntity<?> consultarPorId(@PathVariable Long id){
        ClienteDTO cliente = null;
        HashMap<String, Object> map = new HashMap<>();

        try {
            cliente =  clienteService.findForId(id);
        }catch (DataAccessException e){
            map.put("mensaje", "Error al realizar la consulta a la base de datos");
            map.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().toString()));
            return new ResponseEntity<HashMap<String,Object>>(map, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if(cliente == null){
            map.put("mensaje","El CLiente con el ID: ".concat(id.toString()).concat(" No existe en la BD "));
            return new ResponseEntity<HashMap<String,Object>>(map, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<ClienteDTO>(cliente, HttpStatus.OK);
    }

    @PostMapping ("/clientes")
    public ResponseEntity<?> create(@RequestBody ClienteDTO cliente, @Valid BindingResult result){

        for (int i = 0; i<=300; i++){

            log.info("Entrando a Create " + cliente.toString());
            log.warn("El ID se genera automaticamente en la BD " + i);
            log.error("El ID se genera automaticamente en la BD "+ i);
            log.debug("El ID se genera automaticamente en la BD "+ i);
            log.fatal("El ID se genera automaticamente en la BD "+ i);

        }
        ClienteDTO clienteNew = null;
        //ClienteDTO clientetemp = new ClienteDTO(0,null,null,null,null);
        HashMap<String, Object> map = new HashMap<>();
        //Segunda Opcion para version menores a Java 8
        if (result.hasErrors()) {
            /*List<String> errors = new ArrayList<>();
            for (FieldError fileError: result.getFieldErrors()){
                errors.add("El campo '" + fileError.getField() + "' " + fileError.getDefaultMessage());
            }*/


            // Metodo para Java 8
            List<String> errors = result.getFieldErrors().stream().map(err -> "El campo '" + err.getField() + "' " + err.getDefaultMessage()).collect(Collectors.toList());
            map.put("errores", errors);
            return new ResponseEntity<HashMap<String,Object>>(map, HttpStatus.BAD_REQUEST);
        }
        try {
            clienteNew =  clienteService.save(cliente);
        }catch (DataAccessException e){
            map.put("mensaje", "Error al realizar la insercion a la base de datos");
            map.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().toString()));
            return new ResponseEntity<HashMap<String,Object>>(map, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        map.put("mensaje: ","Cliente Insertado Correctamente");
        map.put("Cliente: ",clienteNew.toString());

        return new ResponseEntity<HashMap<String,Object>>(map, HttpStatus.OK);
    }


    @PutMapping("/clientes/{id}")
    public ResponseEntity<?> update( @RequestBody ClienteDTO cliente,@Valid BindingResult result ,@PathVariable Long id){
        ClienteDTO temp = null;
        HashMap<String, Object> map = new HashMap<>();
        temp = clienteService.findForId(id);
        ClienteDTO clienteActualizado;
        if(result.hasErrors()){
            List<String> errores = result.getFieldErrors().stream().map(err -> "El campo '" + err.getField() + "' " + err.getDefaultMessage())
                    .collect(Collectors.toList());

            map.put("errores", errores);
            return new ResponseEntity<HashMap<String,Object>>(map, HttpStatus.BAD_REQUEST);
        }

        if(temp == null){
            map.put("mensaje","El CLiente con el ID: ".concat(id.toString()).concat(" No existe en la BD "));
            return new ResponseEntity<HashMap<String,Object>>(map, HttpStatus.NOT_FOUND);
        }
        try{
            temp.setNombre(cliente.getNombre());
            temp.setApellido(cliente.getApellido());
            temp.setEmail(cliente.getEmail());
            clienteActualizado = clienteService.save(temp);

        }catch (DataAccessException e){
            map.put("mensaje", "Error al realizar la Actualizacion a la base de datos");
            map.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().toString()));
            return new ResponseEntity<HashMap<String,Object>>(map, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<ClienteDTO>(clienteActualizado, HttpStatus.CREATED);

    }

    @DeleteMapping("/clientes/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id ){
        HashMap<String, Object> map = new HashMap<>();
        try{
            clienteService.delete(id);
        }catch (DataAccessException e){
            map.put("mensaje", "Error al realizar la Actualizacion a la base de datos");
            map.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().toString()));
            return new ResponseEntity<HashMap<String,Object>>(map, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        map.put("mensaje", "Se ha eliminado el Cliente Correctamente");
        return new ResponseEntity<HashMap<String,Object>>(map, HttpStatus.NO_CONTENT);
    }



}
