package co.com.springbootapirest.services;

import co.com.springbootapirest.co.com.springbootapirest.models.Cliente;
import co.com.springbootapirest.co.com.springbootapirest.models.DTO.ClienteDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;

public interface IClienteService {

    public List<Cliente>  findAll();
    public Page<Cliente> findAll(Pageable pageable);
    public ClienteDTO  findForId(Long id);
    public ClienteDTO save(ClienteDTO cliente);
    public void delete (Long id);
}
