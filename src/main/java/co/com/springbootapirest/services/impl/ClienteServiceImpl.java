package co.com.springbootapirest.services.impl;

import co.com.springbootapirest.co.com.springbootapirest.models.Cliente;
import co.com.springbootapirest.co.com.springbootapirest.models.DTO.ClienteDTO;
import co.com.springbootapirest.dao.IClienteDao;
import co.com.springbootapirest.services.IClienteService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Pageable;
import java.util.List;
@Service
public class ClienteServiceImpl implements IClienteService {

    @Autowired
    private IClienteDao clienteDao;
    ObjectMapper mapper = new ObjectMapper();
    Cliente cliente;

    @Override
    @Transactional(readOnly = true)
    public List<Cliente> findAll() {
        return (List<Cliente>) clienteDao.findAll(); }

    @Override
    @Transactional(readOnly = true)
    public Page<Cliente> findAll(Pageable pageable) {
        return clienteDao.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public ClienteDTO findForId(Long id) {
        cliente = clienteDao.findById(id).orElse(null);

        return mapper.convertValue(cliente, ClienteDTO.class);
    }

    @Override
    @Transactional
    public ClienteDTO save(ClienteDTO clienteDTO) {
        cliente = mapper.convertValue(clienteDTO, Cliente.class);
        cliente = clienteDao.save(cliente);

        return mapper.convertValue(cliente, ClienteDTO.class);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        clienteDao.deleteById(id);
    }
}
