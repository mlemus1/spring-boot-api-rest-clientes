package co.com.springbootapirest.services.impl;
import co.com.springbootapirest.co.com.springbootapirest.models.DTO.UsuarioDTO;
import co.com.springbootapirest.dao.IUsuarioDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;

import java.util.stream.Collectors;

@Service
public class UsuarioServiceImpl implements UserDetailsService {

    @Autowired
    private IUsuarioDao usuarioDao;

    private ObjectMapper mapper = new ObjectMapper();

    private Logger logger = LoggerFactory.getLogger(UsuarioServiceImpl.class);

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        UsuarioDTO usuarioDTO = mapper.convertValue(usuarioDao.findByUserName(s), UsuarioDTO.class);
        if(usuarioDTO == null){
        logger.error("Usuario no se '"+ s +"'encontro en la base de Datos ");
        throw new UsernameNotFoundException("Usuario no se '"+ s +"'encontro en la base de Datos ");
        }
        List<GrantedAuthority> authorities = usuarioDTO.getRoles()
                .stream()
                .map(role -> new SimpleGrantedAuthority(role.getUserName()))
                .peek(authority -> logger.info("Role: " + authority.getAuthority()))
                .collect(Collectors.toList());

        return new User(usuarioDTO.getUserName(),usuarioDTO.getPassword(),usuarioDTO.getEnabled(),true, true, true, authorities);
    }
}
